LÖVE is an *awesome* framework you can use to make 3D games in Lua. It's free, open-source, and works on Windows, and Mac OS X.

Documentation
-------------

We use our [wiki][wiki] for documentation.
If you need further help, feel free to ask on our [forums][forums], and last but not least there's the irc channel [#love on OFTC][irc].

Compilation
-----------

###Windows
Use the project files for Visual Studio 2010 located in the platform dir.

###OSX
Use the XCode project in platform/macosx.

For both Windows and OSX there are dependencies available [here][dependencies].

Repository information
----------------------

We use the 'default' branch for development, and therefore it should not be considered stable.
Also used is the 'minor' branch, which is used for features in the next minor version and it is
not our development target (which would be the next revision). (Version numbers formatted major.minor.revision.)

We tag all our releases (since we started using git), and have binary downloads available for them.

Builds
------

There are also unstable/nightly builds:

- For windows they are located [here][winbuilds].

Dependencies
------------

- SDL
- OpenGL
- OpenAL
- Lua / LuaJIT / LLVM-lua
- DevIL with MNG and TIFF
- FreeType
- PhysicsFS
- ModPlug
- mpg123
- Vorbisfile
- Lots of other stuff

[site]: http://love2d.org
[wiki]: http://love2d.org/wiki
[forums]: http://love2d.org/forums
[irc]: irc://irc.oftc.net/love
[dependencies]: http://love2d.org/sdk
[winbuilds]: http://love2d.org/builds